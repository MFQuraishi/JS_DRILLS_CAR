
function lastCar(data){
    return `Last car is a ${data[data.length-1]["car_make"]} ${data[data.length-1]["car_model"]}`;
}

module.exports = {lastCar}