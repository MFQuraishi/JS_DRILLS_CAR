function findByMake(data, args){

    if(!Array.isArray(args)){
        return "argument not an array";
    }

    temp = [];
    for(let i=0; i < data.length-1; i++){
        make = data[i]["car_make"];

        for(let j=0; j<args.length; j++){
            if (make == args[j]){
                temp.push(data[i]["car_model"]);    
            }
        }

    }

    return temp;
}

module.exports = {
    findByMake: findByMake
}