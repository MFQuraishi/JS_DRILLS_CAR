const data = require('./data');
const problem6 = require('./../problem6');

let args = ['BMW', 'Audi'];

let result = problem6.findByMake(data.inventory, args);

console.log(JSON.stringify(result));