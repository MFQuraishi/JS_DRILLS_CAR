function findCarAt(data, n) {

    if(!Number.isInteger(n)){
        return "enter a valid index";
    }

    for (let i=0; i< data.length; i++){
        if (data[i]["id"] == n){
            return `Car ${n} is a ${data[i]["car_year"]} ${data[i]["car_make"]} ${data[i]["car_model"]}`;
        }
    }

    return "not found";

}

module.exports = {
    findCarAt: findCarAt
}