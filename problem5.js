const arr = require('./problem4');

function findYearsGreaterThan(data, n){

    if(!Number.isInteger(n)){
        return "input proper integer for second";
    }

    const yearArr = arr.findAllYears(data);
    temp = [];
    for(let i=0; i<yearArr.length;i++){
        if(yearArr[i] > 2000){
            temp.push(yearArr[i]);
        }
    }    
    return temp;
}

module.exports = {
    findYearsGreaterThan: findYearsGreaterThan
}