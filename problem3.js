function carModels(data){
    temp = [];
    for(let i=0; i<data.length; i++){
        temp.push(data[i]["car_model"]);
    }
    return temp.sort();
}


module.exports = {
    carModels: carModels 
}