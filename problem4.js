function findAllYears(data){
    temp = [];
    for(let i=0; i<data.length; i++){
        temp.push(data[i]["car_year"]);
    }
    return temp.sort();
}


module.exports = {
    findAllYears: findAllYears
}